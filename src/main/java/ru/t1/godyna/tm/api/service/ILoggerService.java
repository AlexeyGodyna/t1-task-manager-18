package ru.t1.godyna.tm.api.service;

public interface ILoggerService {

    void info(final String message);

    void command(final String message);

    void error(final Exception e);

}
