package ru.t1.godyna.tm.api.model;

import ru.t1.godyna.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
