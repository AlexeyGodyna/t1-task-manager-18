package ru.t1.godyna.tm.exception.field;

public class LoginEmptyException extends AbsractFieldException{

    public LoginEmptyException() {
        super("Error! Login is empty...");
    }

}
