package ru.t1.godyna.tm.exception.user;

public class ExistsLoginException extends AbstractUserException {

    public ExistsLoginException() {
        super("Error! Login alredy exists...");
    }

    public ExistsLoginException(String login) {
        super("Error! Login '" + login + "' alredy exists...");
    }

}
