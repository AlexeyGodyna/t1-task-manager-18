package ru.t1.godyna.tm.exception.field;

public class PasswordEmptyException extends AbsractFieldException {

    public PasswordEmptyException() {
        super("Error! Password is empty...");
    }

}
