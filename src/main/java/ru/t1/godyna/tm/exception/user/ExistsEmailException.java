package ru.t1.godyna.tm.exception.user;

public class ExistsEmailException extends AbstractUserException {

    public ExistsEmailException() {
        super("Error! Email alredy exists...");
    }

    public ExistsEmailException(String email) {
        super("Error! Email '" + email + "'alredy exists...");
    }

}
