package ru.t1.godyna.tm.exception.field;

public class IdEmptyException extends AbsractFieldException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
