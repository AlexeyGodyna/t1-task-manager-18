package ru.t1.godyna.tm.exception.field;

public class EmailEmptyException extends AbsractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
