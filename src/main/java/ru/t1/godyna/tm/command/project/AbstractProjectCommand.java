package ru.t1.godyna.tm.command.project;

import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.api.service.IProjectTaskService;
import ru.t1.godyna.tm.command.AbstractCommand;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

}
