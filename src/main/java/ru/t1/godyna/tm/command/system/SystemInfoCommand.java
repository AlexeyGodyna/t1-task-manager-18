package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.util.FormatUtil;

public final class SystemInfoCommand extends AbstractSystemCommand{

    private final String NAME = "info";

    private final String ARGUMENT = "-i";

    private final String DESCRIPTION = "Show system information.";

    @Override
    public void execute() {
        System.out.println("[INFO]");
        final int avaliableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + avaliableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + FormatUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = FormatUtil.formatBytes(maxMemory);
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = maxMemoryCheck ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + FormatUtil.formatBytes(totalMemory));
        final long usageMemory = totalMemory - freeMemory;
        System.out.println("Usage memory: " + FormatUtil.formatBytes(usageMemory));
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
