package ru.t1.godyna.tm.enumerated;

public enum Role {

    USUAL("Usual user"),
    ADMIN("Administrator");

    private String displayName;

    Role (String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }

}
