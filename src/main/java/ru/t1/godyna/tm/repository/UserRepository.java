package ru.t1.godyna.tm.repository;

import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private List<User> users = new ArrayList<>();

    @Override
    public User add(final User user) {
        users.add(user);
        return user;
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public User findById(final String id) {
        for (User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : users) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public User remove(final User user) {
        users.remove(user);
        return user;
    }

    @Override
    public Boolean isLoginExist(String login) {
        return findByLogin(login) != null;
    }

    @Override
    public Boolean isMailExist(String email) {
        return findByEmail(email) != null;
    }

}
